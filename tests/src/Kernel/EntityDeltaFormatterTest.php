<?php

namespace Drupal\Tests\entity_delta_formatter\Kernel;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\entity_delta_formatter\Plugin\Field\FieldFormatter\EntityReferenceDeltaFormatter;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Tests the EntityReferenceDeltaFormatter class.
 */
class EntityDeltaFormatterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_delta_formatter',
  ];

  /**
   * A field item list with test data.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected $fieldItemList;

  /**
   * The container.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * The default configuration.
   *
   * @var array
   */
  protected $defaultConfiguration;

  /**
   * The plugin id.
   *
   * @var string
   */
  protected $pluginId = 'entity_reference_delta_formatter';

  /**
   * The plugin definition.
   *
   * @var array
   */
  protected $pluginDefinition = [
    'field_types' => ['entity_reference'],
    'id' => 'entity_reference_delta_formatter',
    'label' => '',
    'description' => '',
    'class' => 'Drupal\entity_delta_formatter\Plugin\Field\FieldFormatter\EntityReferenceDeltaFormatter',
    'provider' => 'entity_delta_formatter',
    'quickedit' => ['editor' => 'form'],
  ];

  /**
   * The test array of fruits.
   *
   * @var array
   */
  protected $fruits = [
    'apple',
    'banana',
    'cherry',
    'durian',
    'elderberry',
    'fig',
    'grape',
    'huckleberry',
    'jackfruit',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setup(): void {
    parent::setUp();

    // Create the field item list.
    $this->fieldItemList = new FieldItemList(ListDataDefinition::createFromDataType('string'), NULL, NULL);
    foreach ($this->fruits as $fruit) {
      $this->fieldItemList->appendItem($fruit);
    }

    $this->defaultConfiguration = [
      'label' => '',
      'settings' => [
        'view_mode' => 'default',
        'deltas' => '',
        'link' => FALSE,
      ],
      'third_party_settings' => [],
      'type' => 'entity_reference_delta_formatter',
      'field_definition' => new FieldConfig([
        'field_name' => 'fruits',
        'entity_type' => 'grocer',
        'bundle' => 'green_grocer',
      ]),
      'view_mode' => 'full',
    ];
  }

  /**
   * Uses the provided configuration to check functionality.
   *
   * @param array $expected
   *   The expected result.
   * @param array $configuration
   *   The test configuration.
   * @param string $message
   *   The failure message.
   */
  protected function evaluate(array $expected, array $configuration, $message) {
    $formatter = EntityReferenceDeltaFormatter::create($this->container, $configuration, $this->pluginId, $this->pluginDefinition);

    $field_item_list = clone $this->fieldItemList;

    // Filter the field item list.
    $formatter->filterItemsByDelta($field_item_list);
    $actual = [];
    for ($i = 0; $i < $field_item_list->count(); $i++) {
      $actual[] = $field_item_list->get($i)->getValue();
    }
    $this->assertEquals($expected, $actual, $message);
  }

  /**
   * Tests the formatter with a series of different configurations.
   */
  public function testConfigurations() {
    // Get the default configuration.
    $configuration = $this->defaultConfiguration;

    // Test no deltas.
    $message = 'Test no deltas -- Failed.';
    $this->evaluate($this->fruits, $configuration, $message);

    // Test a single positive delta.
    $message = 'Test a single positive delta -- Failed.';
    $configuration['settings']['deltas'] = '2';
    $this->evaluate(['banana'], $configuration, $message);

    // Test a single too high delta.
    $message = 'Test a single too high delta -- Failed.';
    $configuration['settings']['deltas'] = '11';
    $this->evaluate(['jackfruit'], $configuration, $message);

    // Test a single too high delta.
    $message = 'Test a single too high delta -- Failed.';
    $configuration['settings']['deltas'] = '-2';
    $this->evaluate(['huckleberry'], $configuration, $message);

    // Test a single too low delta.
    $message = 'Test a single too low delta -- Failed.';
    $configuration['settings']['deltas'] = '-11';
    $this->evaluate(['apple'], $configuration, $message);

    // Test a range of positive deltas.
    $message = 'Test a range of positive deltas -- Failed.';
    $configuration['settings']['deltas'] = '2_4';
    $expected = ['banana', 'cherry', 'durian'];
    $this->evaluate($expected, $configuration, $message);

    // Test an out of order range of positive deltas.
    $message = 'Test an out of order range of positive deltas -- Failed.';
    $configuration['settings']['deltas'] = '4_2';
    $this->evaluate($expected, $configuration, $message);

    // Test a range of negative deltas.
    $message = 'Test a range of negative deltas -- Failed.';
    $configuration['settings']['deltas'] = '-4_-2';
    $expected = ['fig', 'grape', 'huckleberry'];
    $this->evaluate($expected, $configuration, $message);

    // Test a range of out of order negative deltas.
    $message = 'Test a range of out of order negative deltas -- Failed.';
    $configuration['settings']['deltas'] = '-2_-4';
    $this->evaluate($expected, $configuration, $message);

    // Test a range of mixed positive and negative deltas.
    $message = 'Test a range of mixed positive and negative deltas -- Failed.';
    $configuration['settings']['deltas'] = '5_-3';
    $expected = ['elderberry', 'fig', 'grape'];
    $this->evaluate($expected, $configuration, $message);

    // Test a range of out of order mixed positive and negative deltas.
    $message = 'Test a range of out of order mixed positive and negative deltas -- Failed.';
    $configuration['settings']['deltas'] = '-3_5';
    $this->evaluate($expected, $configuration, $message);

    // Test multiple single deltas.
    $message = 'Test multiple single deltas -- Failed.';
    $configuration['settings']['deltas'] = '1,-1';
    $expected = ['apple', 'jackfruit'];
    $this->evaluate($expected, $configuration, $message);

    // Test a mix of a single delta and a range.
    $message = 'Test a mix of a single delta and a range -- Failed.';
    $configuration['settings']['deltas'] = '1, 3_5';
    $expected = ['apple', 'cherry', 'durian', 'elderberry'];
    $this->evaluate($expected, $configuration, $message);

    // Test whitespaces.
    $message = 'Test whitespaces -- Failed.';
    $configuration['settings']['deltas'] = '1 ,   5,  -1   ';
    $expected = ['apple', 'elderberry', 'jackfruit'];
    $this->evaluate($expected, $configuration, $message);
  }

}
