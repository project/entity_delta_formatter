INTRODUCTION
------------

The Entity Delta Formatter module provides a field formatter for entity
reference fields that displays a selected set of items from a field with
multiple items.


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. Visit:
    https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
    for further information.


CONFIGURATION
-------------

  * Set the field formatter for an entity reference field to
    "Rendered entities by delta".
  * In the configuration menu, specify deltas.
    * Select an item by entering its position in the list starting with 1.
    * Select an item from the end of the list by entering its position from the
    end as a negative number. E.g. "-2" to get the second to last item.
    * Select ranges of items by specifying the inclusive start and end separated
    by "_". E.g. "1_3" to get the first, second and third items or "2_-1" to get
    all but the first items.
    * Multiple selections may be combined using commas. E.g "1_3, -1" to get the
    first, second, third, and last items.

MAINTAINERS
-----------

Current maintainers:
  * George Anderson - https://www.drupal.org/u/geoanders
  * Jon Antoine - https://www.drupal.org/u/jantoine
  * Charles Bamford - https://www.drupal.org/u/c7bamford

This project has been sponsored by:
  * ANTOINE SOLUTIONS
